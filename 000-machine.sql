create table machine
(
    id     int auto_increment
        primary key,
    city   text                 not null,
    enable tinyint(1) default 1 null
);

