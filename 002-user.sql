create table user
(
    user_name char(20)             not null
        primary key,
    phone     char(11)             not null,
    balance   double     default 0 null,
    enable    tinyint(1) default 1 null,
    constraint phone
        unique (phone)
);

