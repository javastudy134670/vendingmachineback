create table product
(
    product_name char(20)             not null
        primary key,
    price        double               not null,
    enable       tinyint(1) default 1 null
);

