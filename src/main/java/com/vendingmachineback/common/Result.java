package com.vendingmachineback.common;

public record Result(String code, String msg, Object data) {
    public static final String CODE_200 = "200";//success
    public static final String CODE_401 = "401";//unauthorized
    public static final String CODE_400 = "400";//parameter error
    public static final String CODE_500 = "500";//system error

    public static Result success() {
        return success(null);
    }

    public static Result success(Object data) {
        return new Result(CODE_200, "", data);
    }

    public static Result error(String code, String msg) {
        return new Result(code, msg, null);
    }
}
