package com.vendingmachineback.entity;

public record User(String user_name, String phone, double balance, boolean enable) {
}
