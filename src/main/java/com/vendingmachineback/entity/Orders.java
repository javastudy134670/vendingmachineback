package com.vendingmachineback.entity;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public record Orders(
        String order_id,
        String user_name,
        int machine_id,
        @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        Date ctime,
        String product_name,
        int product_amount
) {
}
