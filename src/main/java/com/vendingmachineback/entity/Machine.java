package com.vendingmachineback.entity;

public record Machine(
        int id,
        String city,
        boolean enable
) {
}
