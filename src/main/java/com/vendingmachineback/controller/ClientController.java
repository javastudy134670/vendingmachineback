package com.vendingmachineback.controller;

import com.vendingmachineback.common.Result;
import com.vendingmachineback.entity.User;
import com.vendingmachineback.service.ClientService;
import org.springframework.web.bind.annotation.*;

@RestController//返回的对象转化为json字符串
@RequestMapping("/client")
public class ClientController {
    private final ClientService clientService;

    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @PostMapping("/buy")
    public Result buy(
            @RequestParam
            String user_name,
            @RequestParam
            int machine_id,
            @RequestParam
            String product_name,
            @RequestParam
            int amount
    ) {
        if (clientService.userDisabled(user_name)) {
            return Result.error(Result.CODE_400, "user was disabled");
        }
        if (clientService.machineDisabled(machine_id)) {
            return Result.error(Result.CODE_400, "machine was disabled");
        }
        if (clientService.productDisabled(product_name)) {
            return Result.error(Result.CODE_400, "product was disabled");
        }
        if (amount <= 0) {
            return Result.error(Result.CODE_401, "amount must >0, got " + amount);
        }
        try {
            clientService.buy(user_name, machine_id, product_name, amount);
        } catch (Exception exception) {
            return Result.error(Result.CODE_400, "permission denied");
        }
        return Result.success();
    }

    @PostMapping("/recharge")
    public Result recharge(
            @RequestParam
            String user_name,
            @RequestParam
            double amount
    ) {
        if (clientService.userDisabled(user_name)) {
            return Result.error(Result.CODE_400, "user was disabled");
        }
        if (amount <= 0) {
            return Result.error(Result.CODE_401, "amount must >0, got " + amount);
        }
        try {
            clientService.recharge(user_name, amount);
        } catch (Exception exception) {
            return Result.error(Result.CODE_500, "inner error while recharge");
        }
        return Result.success();
    }

    @PostMapping("/login")
    public Result login(
            @RequestParam
            String user_name
    ) {
        User user = clientService.login(user_name);
        if (user == null) {
            return Result.error(Result.CODE_400, "user not exist");
        }
        return Result.success(user);
    }

    @GetMapping("/machine-list")
    public Result getMachineList() {
        return Result.success(clientService.getMachineList());
    }

    @GetMapping("/product-list")
    public Result getProductList(
            @RequestParam
            int machine_id
    ) {
        return Result.success(clientService.getProductList(machine_id));
    }

    @GetMapping("/price")
    public Result getPrice(
            @RequestParam
            String product_name
    ){
        Double price = clientService.getPrice(product_name);
        if(price==null){
            return Result.error(Result.CODE_400,"invalid product");
        }
        return Result.success(price);
    }
}
