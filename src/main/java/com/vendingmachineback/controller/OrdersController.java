package com.vendingmachineback.controller;

import com.vendingmachineback.common.Result;
import com.vendingmachineback.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/orders")
public class OrdersController {
    private final OrdersService ordersService;

    @Autowired
    public OrdersController(OrdersService ordersService) {
        this.ordersService = ordersService;
    }

    @GetMapping("/get")
    public Result get(
            @RequestParam(required = false)
            Integer machine_id,
            @RequestParam(required = false)
            String user_name,
            @RequestParam(required = false)
            String product_name
    ) {
        if (machine_id != null) {
            return Result.success(ordersService.getByMachineId(machine_id));
        }
        if (user_name != null) {
            return Result.success(ordersService.getByUser(user_name));
        }
        if (product_name != null) {
            return Result.success(ordersService.getByProduct(product_name));
        }
        return Result.success(ordersService.getAll());
    }
}
