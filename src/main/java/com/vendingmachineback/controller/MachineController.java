package com.vendingmachineback.controller;

import com.vendingmachineback.common.Result;
import com.vendingmachineback.service.MachineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/machine")
public class MachineController {
    private final MachineService machineService;

    @Autowired
    public MachineController(MachineService machineService) {
        this.machineService = machineService;
    }

    @GetMapping("/all")
    public Result all() {
        return Result.success(machineService.getAll());
    }

    @PostMapping("/create")
    public Result create(
            @RequestParam
            String city
    ) {
        machineService.insert(city);
        return Result.success();
    }

    @PostMapping("/set-enable")
    public Result disable(
            @RequestParam
            int machine_id,
            @RequestParam
            boolean enable
    ) {
        if (machineService.getById(machine_id) == null) {
            return Result.error(Result.CODE_401, "machine id(%d) not found".formatted(machine_id));
        }
        try {
            if (enable) {
                machineService.enable(machine_id);
            } else {
                machineService.disable(machine_id);
            }
        } catch (Exception exception) {
            return Result.error(Result.CODE_500, "inner error while set enable machine id(%d)".formatted(machine_id));
        }
        return Result.success();
    }

    @PostMapping("/change-city")
    public Result change_city(
            @RequestParam
            int machine_id,
            @RequestParam
            String city
    ) {
        if (machineService.getById(machine_id) == null) {
            return Result.error(Result.CODE_401, "machine id(%d) not found".formatted(machine_id));
        }
        if (city.isBlank()) {
            return Result.error(Result.CODE_401, "city is blank");
        }
        try {
            machineService.changeCity(machine_id, city);
        } catch (Exception exception) {
            return Result.error(Result.CODE_500, "inner error while change city of machine(%d)".formatted(machine_id));
        }
        return Result.success();
    }
}
