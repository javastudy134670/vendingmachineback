package com.vendingmachineback.controller;

import com.vendingmachineback.common.Result;
import com.vendingmachineback.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/all")
    public Result getAll() {
        return Result.success(userService.getAll());
    }

    @PostMapping("/register")
    public Result register(
            @RequestParam
            String user_name,
            @RequestParam
            String phone
    ) {
        if (userService.getByUsername(user_name) != null) {
            return Result.error(Result.CODE_400, "username repeat");
        }
        if (userService.getByPhone(phone) != null) {
            return Result.error(Result.CODE_400, "phone repeat");
        }
        try {
            userService.insert(user_name, phone);
        } catch (Exception exception) {
            return Result.error(Result.CODE_500, "inner error while register\n" + exception.getMessage());
        }
        return Result.success();
    }

    @PostMapping("/set-enable")
    public Result disable(
            @RequestParam
            String user_name,
            @RequestParam
            boolean enable
    ) {
        if (userService.getByUsername(user_name) == null) {
            return Result.error(Result.CODE_400, "user not exist");
        }
        try {
            if(enable){
                userService.enable(user_name);
            }else {
                userService.disable(user_name);
            }
        } catch (Exception exception) {
            return Result.error(Result.CODE_500, "inner error while set enable user\n" + exception.getMessage());
        }
        return Result.success();
    }

}
