package com.vendingmachineback.controller;

import com.vendingmachineback.common.Result;
import com.vendingmachineback.entity.Inventory;
import com.vendingmachineback.service.InventoryService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/inventory")
public class InventoryController {
    private final InventoryService inventoryService;

    public InventoryController(InventoryService inventoryService) {
        this.inventoryService = inventoryService;
    }

    @GetMapping("/get")
    Result get(
            @RequestParam(required = false)
            Integer machine_id,
            @RequestParam(required = false)
            String product_name
    ) {
        if (machine_id == null) {
            if (product_name == null) {
                return Result.success(inventoryService.getAll());
            } else {
                return Result.success(inventoryService.getByProduct(product_name));
            }
        } else if (product_name == null) {
            return Result.success(inventoryService.getByMachine(machine_id));
        } else {
            return Result.success(inventoryService.get(machine_id, product_name));
        }
    }

    @PostMapping("/update")
    Result update(
            @RequestParam
            int machine_id,
            @RequestParam
            String product_name,
            @RequestParam
            int amount
    ) {
        if (inventoryService.get(machine_id, product_name) == null) {
            try {
                inventoryService.insert(machine_id, product_name, amount);
            } catch (Exception exception) {
                return Result.error(Result.CODE_500, "inner error while insert inventory\n" + new Inventory(machine_id, product_name, amount) + exception.getMessage());
            }
        } else {
            try {
                inventoryService.update(machine_id, product_name, amount);
            } catch (Exception exception) {
                return Result.error(Result.CODE_500, "inner error while update\n" + exception.getMessage());
            }
        }
        return Result.success();
    }
}
