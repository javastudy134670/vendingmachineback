package com.vendingmachineback.controller;

import com.vendingmachineback.common.Result;
import com.vendingmachineback.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product")
public class ProductController {
    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/all")
    public Result getAll() {
        return Result.success(productService.getAll());
    }

    @PostMapping("/new")
    public Result launchNew(
            @RequestParam
            String product_name,
            @RequestParam
            double price
    ) {
        if (productService.getByName(product_name) != null) {
            return Result.error(Result.CODE_400, "The product '%s' already exists, please do not add it repeatedly".formatted(product_name));
        }
        try {
            productService.insert(product_name, price);
        } catch (Exception exception) {
            return Result.error(Result.CODE_500, "inner error while launching new product\n" + exception.getMessage());
        }
        return Result.success();
    }

    @PostMapping("/set-enable")
    public Result off(
            @RequestParam
            String product_name,
            @RequestParam
            boolean enable
    ) {
        if (productService.getByName(product_name) == null) {
            return Result.error(Result.CODE_401, "'%s' not exists".formatted(product_name));
        }
        try {
            if (enable) {
                productService.on(product_name);
            } else {
                productService.off(product_name);
            }
        } catch (Exception exception) {
            return Result.error(Result.CODE_500, "inner error while take off product '%s'\n".formatted(product_name) + exception.getMessage());
        }
        return Result.success();
    }

    @PostMapping("/change-price")
    public Result change_price(
            @RequestParam
            String product_name,
            @RequestParam
            double price
    ) {
        if (productService.getByName(product_name) == null) {
            return Result.error(Result.CODE_401, "'%s' not exists".formatted(product_name));
        }
        if (price <= 0) {
            return Result.error(Result.CODE_400, "price invalid");
        }
        try {
            productService.change_price(product_name, price);
        } catch (Exception exception) {
            return Result.error(Result.CODE_500, "inner error while change price\n" + exception.getMessage());
        }
        return Result.success();
    }
}
