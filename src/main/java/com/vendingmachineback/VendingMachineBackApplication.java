package com.vendingmachineback;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.vendingmachineback.mapper")//扫描所有mapper
public class VendingMachineBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(VendingMachineBackApplication.class, args);
    }

}
