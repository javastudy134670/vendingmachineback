package com.vendingmachineback.service;

import com.vendingmachineback.entity.Inventory;
import com.vendingmachineback.mapper.InventoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InventoryService {
    private final InventoryMapper inventoryMapper;

    @Autowired
    public InventoryService(InventoryMapper inventoryMapper) {
        this.inventoryMapper = inventoryMapper;
    }

    public void insert(int machine_id, String product_name, int amount) {
        inventoryMapper.insert(machine_id, product_name, amount);
    }

    public List<Inventory> getByMachine(int machine_id) {
        return inventoryMapper.selectByMachine(machine_id);
    }

    public List<Inventory> getByProduct(String product_name) {
        return inventoryMapper.selectByProduct(product_name);
    }

    public List<Inventory> getAll() {
        return inventoryMapper.selectAll();
    }

    public Inventory get(int machine_id, String product_name) {
        return inventoryMapper.select(machine_id, product_name);
    }

    public void update(int machine_id, String product_name, int amount) {
        inventoryMapper.update(machine_id, product_name, amount);
    }
}
