package com.vendingmachineback.service;

import com.vendingmachineback.entity.Orders;
import com.vendingmachineback.mapper.OrdersMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrdersService {
    private final OrdersMapper ordersMapper;

    @Autowired
    public OrdersService(OrdersMapper ordersMapper) {
        this.ordersMapper = ordersMapper;
    }

    public List<Orders> getAll() {
        return ordersMapper.selectAll();
    }

    public List<Orders> getByMachineId(int machine_id){
        return ordersMapper.selectByMachineId(machine_id);
    }

    public List<Orders> getByUser(String user_name){
        return ordersMapper.selectByUsername(user_name);
    }

    public List<Orders> getByProduct(String product_name){
        return ordersMapper.selectByProduct(product_name);
    }
}
