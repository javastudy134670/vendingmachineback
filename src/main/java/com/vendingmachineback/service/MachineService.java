package com.vendingmachineback.service;

import com.vendingmachineback.entity.Machine;
import com.vendingmachineback.mapper.MachineMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MachineService {
    private final MachineMapper machineMapper;

    @Autowired
    public MachineService(MachineMapper machineMapper) {
        this.machineMapper = machineMapper;
    }

    public List<Machine> getAll() {
        return machineMapper.selectAll();
    }

    public Machine getById(int id) {
        return machineMapper.selectById(id);
    }

    public void insert(String city) {
        machineMapper.insert(city);
    }

    public void disable(int id) {
        machineMapper.disable(id);
    }

    public void enable(int id) {
        machineMapper.enable(id);
    }

    public void changeCity(int id, String city) {
        machineMapper.changeCity(id, city);
    }
}
