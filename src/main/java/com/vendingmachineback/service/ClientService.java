package com.vendingmachineback.service;

import com.vendingmachineback.entity.Inventory;
import com.vendingmachineback.entity.Machine;
import com.vendingmachineback.entity.Product;
import com.vendingmachineback.entity.User;
import com.vendingmachineback.mapper.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ClientService {
    private final UserMapper userMapper;
    private final InventoryMapper inventoryMapper;
    private final ProductMapper productMapper;
    private final OrdersMapper ordersMapper;
    private final MachineMapper machineMapper;

    public ClientService(UserMapper userMapper, InventoryMapper inventoryMapper, ProductMapper productMapper, OrdersMapper ordersMapper, MachineMapper machineMapper) {
        this.userMapper = userMapper;
        this.inventoryMapper = inventoryMapper;
        this.productMapper = productMapper;
        this.ordersMapper = ordersMapper;
        this.machineMapper = machineMapper;
    }

    @Transactional
    public void buy(String user_name, int machine_id, String product_name, int amount) {
        User user = userMapper.selectByUsername(user_name);
        Inventory inventory = inventoryMapper.select(machine_id, product_name);
        Product product = productMapper.selectByName(product_name);
        userMapper.updateBalance(user_name, user.balance() - product.price() * amount);
        inventoryMapper.update(machine_id, product_name, inventory.amount() - amount);
        ordersMapper.insert(user_name, machine_id, new Date(), product_name, amount);
    }

    @Transactional
    public void recharge(String user_name, double amount) {
        User user = userMapper.selectByUsername(user_name);
        userMapper.updateBalance(user_name, user.balance() + amount);
    }

    public User login(String user_name) {
        return userMapper.selectByUsername(user_name);
    }

    public List<Machine> getMachineList() {
        return machineMapper.selectAllAvailable();
    }

    public List<Inventory> getProductList(int machine_id) {
        List<Inventory> inventoryList = inventoryMapper.selectByMachine(machine_id);
        List<Inventory> ret = new ArrayList<>();
        inventoryList.forEach(each -> {
            if (!productDisabled(each.product_name())) {
                ret.add(each);
            }
        });
        return ret;
    }

    public boolean userDisabled(String user_name) {
        User user = userMapper.selectByUsername(user_name);
        if (user == null) {
            return true;
        }
        return !user.enable();
    }

    public boolean productDisabled(String product_name) {
        Product product = productMapper.selectByName(product_name);
        if (product == null) {
            return true;
        }
        return !product.enable();
    }

    public boolean machineDisabled(int machine_id) {
        Machine machine = machineMapper.selectById(machine_id);
        if (machine == null) {
            return true;
        }
        return !machine.enable();
    }

    public Double getPrice(String product_name) {
        Product product = productMapper.selectByName(product_name);
        if (product == null) {
            return null;
        }
        return productMapper.selectByName(product_name).price();
    }
}
