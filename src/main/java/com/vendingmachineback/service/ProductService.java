package com.vendingmachineback.service;

import com.vendingmachineback.entity.Product;
import com.vendingmachineback.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
    private final ProductMapper productMapper;

    @Autowired
    public ProductService(ProductMapper productMapper) {
        this.productMapper = productMapper;
    }

    public List<Product> getAll() {
        return productMapper.selectAll();
    }

    public Product getByName(String product_name) {
        return productMapper.selectByName(product_name);
    }

    public void insert(String product_name, double price) {
        productMapper.insert(product_name, price);
    }

    public void off(String product_name) {
        productMapper.disable(product_name);
    }

    public void on(String product_name) {
        productMapper.enable(product_name);
    }

    public void change_price(String product_name, double price) {
        productMapper.change_price(product_name, price);
    }
}
