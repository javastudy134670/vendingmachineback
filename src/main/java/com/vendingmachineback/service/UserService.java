package com.vendingmachineback.service;

import com.vendingmachineback.entity.User;
import com.vendingmachineback.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    private final UserMapper userMapper;

    @Autowired
    public UserService(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    public List<User> getAll() {
        return userMapper.selectAll();
    }

    public User getByUsername(String username) {
        return userMapper.selectByUsername(username);
    }

    public User getByPhone(String phone) {
        return userMapper.selectByPhone(phone);
    }

    public void insert(String user_name, String phone) {
        userMapper.insert(user_name, phone);
    }

    public void disable(String user_name) {
        userMapper.disable(user_name);
    }

    public void enable(String user_name){
        userMapper.enable(user_name);
    }

    public void updateBalance(String user_name, double balance) {
        userMapper.updateBalance(user_name, balance);
    }
}
