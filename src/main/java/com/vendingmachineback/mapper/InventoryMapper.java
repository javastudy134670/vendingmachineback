package com.vendingmachineback.mapper;

import com.vendingmachineback.entity.Inventory;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface InventoryMapper {
    @Insert("insert into inventory(machine_id, product_name, amount) values(#{machine_id},#{product_name},#{amount})")
    void insert(int machine_id, String product_name, int amount);

    @Select("select * from inventory")
    List<Inventory> selectAll();

    @Select("select * from inventory where machine_id=#{machine_id}")
    List<Inventory> selectByMachine(int machine_id);

    @Select("select * from inventory where product_name=#{product_name}")
    List<Inventory> selectByProduct(String product_name);

    @Select("select * from inventory where machine_id=#{machine_id} and product_name=#{product_name}")
    Inventory select(int machine_id, String product_name);

    @Update("update inventory set amount=#{amount} where machine_id=#{machine_id} and product_name=#{product_name}")
    void update(int machine_id, String product_name, int amount);
}
