package com.vendingmachineback.mapper;

import com.vendingmachineback.entity.Product;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductMapper {
    @Insert("insert into product(product_name, price) values(#{product_name}, #{price})")
    void insert(String product_name, double price);

    /**
     * do not delete a product easily
     * set its "enable" to false instead
     *
     * @param product_name the name of the product which is about te be deleted
     */
    @Delete("delete from product where product_name=#{product_name}")
    void delete(String product_name);

    @Select("select * from product")
    List<Product> selectAll();

    @Select("select * from product where product_name=#{product_name}")
    Product selectByName(String product_name);

    @Update("update product set price=#{price}, enable=#{enable} where product_name=#{product_name}")
    void update(String product_name, double price, boolean enable);

    @Update("update product set enable=false where product_name=#{product_name}")
    void disable(String product_name);

    @Update("update product set enable=true where product_name=#{product_name}")
    void enable(String product_name);

    @Update("update product set price=#{price} where product_name=#{product_name}")
    void change_price(String product_name, double price);
}
