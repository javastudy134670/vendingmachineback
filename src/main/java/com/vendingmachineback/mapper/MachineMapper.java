package com.vendingmachineback.mapper;

import com.vendingmachineback.entity.Machine;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface MachineMapper {
    @Insert("insert into machine(city) values(#{city})")
    void insert(String city);

    /**
     * do not delete a machine easily
     * set its "enable" to false instead
     *
     * @param id id of machine which is about to be deleted
     */
    @Deprecated
    @Delete("delete from machine where id=#{id}")
    void delete(int id);

    @Select("select * from machine where id=#{id}")
    Machine selectById(int id);

    @Select("select * from machine")
    List<Machine> selectAll();

    @Select("select * from machine where enable=true")
    List<Machine> selectAllAvailable();

    @Update("update machine set city=#{city}, enable=#{enable} where id=#{id}")
    void update(int id, String city, boolean enable);

    @Update("update machine set enable=false where id=#{id}")
    void disable(int id);

    @Update("update machine set enable=true where id=#{id}")
    void enable(int id);

    @Update("update machine set city=#{city} where id=#{id}")
    void changeCity(int id, String city);
}
