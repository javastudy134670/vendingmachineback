package com.vendingmachineback.mapper;

import com.vendingmachineback.entity.Orders;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;

/**
 * only insert and select operations are permitted
 * delete and update are not allowed
 *
 * @author Xingnan
 */
@Mapper
public interface OrdersMapper {
    @Insert("insert into orders(user_name, machine_id, ctime, product_name, product_amount) values(#{user_name},#{machine_id},#{ctime},#{product_name},#{product_amount})")
    void insert(String user_name, int machine_id, Date ctime, String product_name, int product_amount);

    @Select("select * from orders where order_id=#{order_id}")
    Orders selectById(int order_id);

    @Select("select * from orders")
    List<Orders> selectAll();

    @Select("select * from orders where user_name=#{user_name}")
    List<Orders> selectByUsername(String user_name);

    @Select("select * from orders where machine_id=#{machine_id}")
    List<Orders> selectByMachineId(int machine_id);

    @Select("select * from orders where product_name=#{product_name}")
    List<Orders> selectByProduct(String product_name);
}
