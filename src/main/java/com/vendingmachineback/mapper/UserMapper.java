package com.vendingmachineback.mapper;

import com.vendingmachineback.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserMapper {
    @Insert("insert into user (user_name, phone) values (#{user_name}, #{phone})")
    void insert(String user_name, String phone);

    /**
     * do not delete a user easily
     * set its "enable" to false instead
     *
     * @param user_name username who is about to be deleted
     */
    @Deprecated
    @Delete("delete from user where user_name=#{user_name}")
    void delete(String user_name);

    @Select("select * from user where user_name=#{user_name}")
    User selectByUsername(String user_name);

    @Select("select * from user where phone=#{phone}")
    User selectByPhone(String phone);

    @Select("select * from user")
    List<User> selectAll();

    @Update("update user set phone=#{phone}, balance=#{balance}, enable=#{enable} where user_name=#{user_name}")
    void update(String user_name, String phone, double balance, boolean enable);

    @Update("update user set enable=false where user_name=#{user_name}")
    void disable(String user_name);

    @Update("update user set enable=true where user_name=#{user_name}")
    void enable(String user_name);

    @Update("update user set balance=#{balance} where user_name=#{user_name}")
    void updateBalance(String user_name, double balance);
}
