package com.vendingmachineback;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = VendingMachineBackApplicationTests.class)
class VendingMachineBackApplicationTests {

    @Test
    void contextLoads() {
    }

}
