create table inventory
(
    machine_id   int           not null,
    product_name char(20)      not null,
    amount       int default 0 null,
    primary key (machine_id, product_name),
    constraint inventory_ibfk_1
        foreign key (machine_id) references machine (id),
    constraint inventory_ibfk_2
        foreign key (product_name) references product (product_name),
    check (`amount` >= 0)
);

