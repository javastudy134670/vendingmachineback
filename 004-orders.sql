create table orders
(
    order_id       int auto_increment
        primary key,
    user_name      char(20) not null,
    machine_id     int      not null,
    ctime          datetime not null,
    product_name   char(20) not null,
    product_amount int      not null,
    constraint orders_ibfk_1
        foreign key (user_name) references user (user_name),
    constraint orders_ibfk_2
        foreign key (machine_id) references machine (id),
    constraint orders_ibfk_3
        foreign key (product_name) references product (product_name),
    check (`product_amount` > 0)
);

create index machine_id
    on orders (machine_id);

create index product_name
    on orders (product_name);

create index user_name
    on orders (user_name);

